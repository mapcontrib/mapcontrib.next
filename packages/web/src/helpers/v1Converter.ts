import * as uuid from 'uuid/v4';

import * as MC from 'src/types/mapcontrib';
import * as MC1 from 'src/types/mapcontribV1';

interface IConvertedTheme {
  dataSources: MC.DataSource[];
  theme: MC.Theme;
  themeLayers: MC.ThemeLayer[];
  themeVisualizationLayers: MC.ThemeVisualizationLayer[];
}

export function convertV1Theme(v1Theme: MC1.Theme): IConvertedTheme {
  const theme = buildTheme(v1Theme);
  const dataSources: MC.DataSource[] = [];
  const themeLayers: MC.ThemeLayer[] = [];
  const themeVisualizationLayers: MC.ThemeVisualizationLayer[] = [];

  for (const v1Layer of v1Theme.layers) {
    const dataSourceId = uuid();
    const layerId = v1Layer.uuid;
    const visualizationLayerId = uuid();
    const themeId = theme.uuid;

    const dataSource: MC.DataSourceBase = {
      hasCache: v1Layer.cache,
      id: dataSourceId,
      layerId,
      themeId,
      uuid: dataSourceId,
    };

    if (v1Layer.type === MC1.LayerTypeId.overpass) {
      dataSources.push({
        ...dataSource,
        minZoom: v1Layer.minZoom,
        query: v1Layer.overpassRequest,
        type: MC.DataSourceType.OVERPASS,
      });
    } else {
      dataSources.push(dataSource as MC.DataSourceOther);
    }

    const themevisualizationLayer = {
      id: visualizationLayerId,
      themeId,
      type: buildThemevisualizationLayerType(v1Layer.rootLayerType),
      uuid: visualizationLayerId,
    };

    if (themevisualizationLayer.type === MC.DataVisualizationType.HEAT) {
      themeVisualizationLayers.push({
        ...(themevisualizationLayer as MC.ThemeVisualizationLayerHeat),
        heatBlur: v1Layer.heatBlur,
        heatMax: v1Layer.heatMax,
        heatMaxZoom: v1Layer.heatMaxZoom,
        heatMinOpacity: v1Layer.heatMinOpacity,
        heatRadius: v1Layer.heatRadius,
      });
    } else {
      themeVisualizationLayers.push(
        themevisualizationLayer as MC.ThemeVisualizationLayerBasic,
      );
    }

    const themeLayer = {
      creationDate: new Date(v1Layer.creationDate),
      description: v1Layer.description,
      featureInfoTemplate: v1Layer.popupContent,
      id: layerId,
      isVisible: v1Layer.visible,
      markerColor: buildMarkerColor(v1Layer.markerColor),
      markerIcon: v1Layer.markerIcon,
      markerIconType: buildMarkerIconType(v1Layer.markerIconType),
      markerIconUrl: v1Layer.markerIconUrl || undefined,
      markerShape: buildMarkerShape(v1Layer.markerShape),
      modificationDate: new Date(v1Layer.modificationDate),
      name: v1Layer.name,
      order: v1Layer.order,
      themeId,
      uuid: layerId,
      visualizationLayerId: themevisualizationLayer.uuid,
    };

    themeLayers.push(themeLayer);
  }

  return {
    dataSources,
    theme,
    themeLayers,
    themeVisualizationLayers,
  };
}

function buildTheme(v1Theme: MC1.Theme): MC.Theme {
  const maxZoomLevel = parseInt((v1Theme.maxZoomLevel || '').toString(), 10);
  const minZoomLevel = parseInt((v1Theme.minZoomLevel || '').toString(), 10);

  return {
    center: v1Theme.center,
    creationDate: new Date(v1Theme.creationDate),
    description: v1Theme.description,
    featureInfoDisplay: buildFeatureInfoDisplay(v1Theme.infoDisplay),
    fragment: v1Theme.fragment,
    geocoder: buildGeocoder(v1Theme.geocoder),
    id: v1Theme._id,
    isAutoCentered: v1Theme.autoCenter,
    maxZoomLevel,
    minZoomLevel,
    modificationDate: new Date(v1Theme.modificationDate),
    movementRadius: buildMovementRadius(v1Theme.movementRadius),
    name: v1Theme.name,
    tiles: v1Theme.tiles,
    uuid: uuid(),
    zoomLevel: v1Theme.zoomLevel,
  };
}

function buildFeatureInfoDisplay(infoDisplay: MC1.InfoDisplay) {
  switch (infoDisplay) {
    case MC1.InfoDisplay.column:
      return MC.FeatureInformationDisplayType.SIDEBAR;
    case MC1.InfoDisplay.modal:
      return MC.FeatureInformationDisplayType.MODAL;
    case MC1.InfoDisplay.popup:
      return MC.FeatureInformationDisplayType.POPUP;
  }
}

function buildGeocoder(geocoder: MC1.Geocoder) {
  switch (geocoder) {
    case MC1.Geocoder.nominatim:
      return MC.Geocoder.NOMINATIM;
    case MC1.Geocoder.photon:
      return MC.Geocoder.PHOTON;
  }
}

function buildMovementRadius(movementRadius?: MC1.MovementRadius) {
  if (movementRadius) {
    return parseInt(movementRadius.toString(), 10);
  }

  return undefined;
}

// function buildDataSourceType(layerTypeId: MC1.LayerTypeId) {
//   switch (layerTypeId) {
//     case MC1.LayerTypeId.csv:
//       return MC.DataSourceType.CSV;
//     case MC1.LayerTypeId.geojson:
//       return MC.DataSourceType.GEOJSON;
//     case MC1.LayerTypeId.gpx:
//       return MC.DataSourceType.GPX;
//     case MC1.LayerTypeId.osmose:
//       return MC.DataSourceType.OSMOSE;
//     case MC1.LayerTypeId.overpass:
//       return MC.DataSourceType.OVERPASS;
//   }
// }

function buildMarkerColor(markerColor: MC1.Color) {
  switch (markerColor) {
    case MC1.ColorName.green:
      return { h: 145, s: 68, l: 35 };
    case MC1.ColorName.blue:
      return { h: 195, s: 75, l: 55 };
    case MC1.ColorName.purple:
      return { h: 329, s: 24, l: 27 };
    case MC1.ColorName.red:
      return { h: 360, s: 76, l: 62 };
    case MC1.ColorName.orange:
      return { h: 34, s: 94, l: 54 };
    case MC1.ColorName.yellow:
      return { h: 53, s: 100, l: 49 };
    case MC1.ColorName.brown:
      return { h: 37, s: 100, l: 35 };
    case MC1.ColorName.white:
      return { h: 0, s: 0, l: 100 };
    case MC1.ColorName.gray:
      return { h: 0, s: 0, l: 93 };
    case MC1.ColorName.darkGray:
      return { h: 0, s: 0, l: 89 };
    case MC1.ColorName.black:
      return { h: 0, s: 0, l: 0 };
    default:
      const { h, s, l } = markerColor as MC.HSLColor;

      return {
        h: Math.round(h),
        l: Math.round(l),
        s: Math.round(s),
      };
  }
}

function buildMarkerIconType(markerIconType: MC1.MarkerIconType) {
  switch (markerIconType) {
    case MC1.MarkerIconType.external:
      return MC.MarkerIconType.EXTERNAL;
    case MC1.MarkerIconType.library:
      return MC.MarkerIconType.LIBRARY;
  }
}

function buildMarkerShape(markerShape: MC1.MarkerShapeId) {
  switch (markerShape) {
    case MC1.MarkerShapeId.marker1:
      return MC.MarkerShape.POINTER_CLASSIC;
    case MC1.MarkerShapeId.marker2:
      return MC.MarkerShape.POINTER_CLASSIC_THIN;
    case MC1.MarkerShapeId.marker3:
      return MC.MarkerShape.POINTER_CIRCLE_PIN;
  }
}

function buildThemevisualizationLayerType(rootLayerType: MC1.RootLayerType) {
  switch (rootLayerType) {
    case MC1.RootLayerType.heat:
      return MC.DataVisualizationType.HEAT;
    case MC1.RootLayerType.markerCluster:
      return MC.DataVisualizationType.CLUSTER;
    default:
      return MC.DataVisualizationType.BASIC;
  }
}

export function buildApiUrlFromV1ThemeUrl(uri: string) {
  return uri.replace(/^(\w+:\/\/.*)\/t\/(\w{6}).*?$/, '$1/api/theme/$2');
}
