import { OffMapMarker } from 'osm-ui-react';
import * as React from 'react';

import * as MC from 'src/types/mapcontrib';

export const buildOffMapMarker = (themeLayer: MC.ThemeLayer) => (
  <OffMapMarker
    color={buildMarkerColor(themeLayer)}
    icon={buildMarkerIcon(themeLayer)}
    shape={themeLayer.markerShape}
  />
);

export const buildMarkerColor = (themeLayer: MC.ThemeLayer) => {
  const { h, s, l } = themeLayer.markerColor;
  return `hsl(${h}, ${s}%, ${l}%)`;
};

export const buildMarkerIcon = (themeLayer: MC.ThemeLayer) => {
  return themeLayer.markerIconType === MC.MarkerIconType.LIBRARY
    ? themeLayer.markerIcon
    : '';
};
