import { osmose } from 'src/helpers/requests.js';
import { addCategories } from 'src/actions/osmose.js';

export const fetchOsmoseCategories = () => {
  return dispatch =>
    osmose
      .fetchItemCategories()
      .then(categories => dispatch(addCategories(categories)));
};
