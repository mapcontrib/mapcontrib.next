import { Actions, TActions } from './actions';

export interface IBridgeState {
  mergeMarkerClusters: boolean;
}

const initialState: IBridgeState = {
  mergeMarkerClusters: true,
};

export default function reducer(state = initialState, action: TActions) {
  switch (action.type) {
    case Actions.SET_MARKER_CLUSTERS_MERGE:
      return {
        ...state,
        mergeMarkerClusters: action.mergeMarkerClusters,
      };

    default:
      return state;
  }
}
