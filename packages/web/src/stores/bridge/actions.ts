export enum Actions {
  SET_MARKER_CLUSTERS_MERGE = 'bridge/SET_MARKER_CLUSTERS_MERGE',
}

export interface ISetMarkerClustersMerge {
  mergeMarkerClusters: boolean;
  type: Actions.SET_MARKER_CLUSTERS_MERGE;
}
export const setMarkerClustersMerge = (
  mergeMarkerClusters: boolean,
): ISetMarkerClustersMerge => ({
  mergeMarkerClusters,
  type: Actions.SET_MARKER_CLUSTERS_MERGE,
});

export type TActions = ISetMarkerClustersMerge;
