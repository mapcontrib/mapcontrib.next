import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { ForkEffect } from 'redux-saga/effects';

type RootSaga = () => IterableIterator<ForkEffect[]>;

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();

function configureStore(reducers = {}, initialState = {}, rootSaga?: RootSaga) {
  const store = createStore(
    combineReducers(reducers),
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
  );

  if (rootSaga) {
    sagaMiddleware.run(rootSaga);
  }

  return store;
}

export default configureStore;
