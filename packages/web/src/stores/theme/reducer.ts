import * as MC from 'src/types/mapcontrib';
import { Actions, TActions } from './actions';

export interface IThemeState {
  currentCenter: MC.LatLng;
  currentZoomLevel: number;
  dataSources: MC.DataSource[];
  isLoaded: boolean;
  isLoading: boolean;
  theme: MC.Theme;
  themeVisualizationLayers: MC.ThemeVisualizationLayer[];
  themeLayers: MC.ThemeLayer[];
  v1ThemeUrl: string;
}

const initialState: IThemeState = {
  currentCenter: { lat: 0, lng: 0 },
  currentZoomLevel: 14,
  dataSources: [],
  isLoaded: false,
  isLoading: false,
  theme: {
    center: { lat: 0, lng: 0 },
    creationDate: new Date(),
    description: '',
    featureInfoDisplay: MC.FeatureInformationDisplayType.SIDEBAR,
    fragment: '',
    geocoder: MC.Geocoder.PHOTON,
    id: '',
    isAutoCentered: false,
    maxZoomLevel: 0,
    minZoomLevel: 0,
    modificationDate: new Date(),
    name: '',
    tiles: [],
    uuid: '',
    zoomLevel: 0,
  },
  themeLayers: [],
  themeVisualizationLayers: [],
  v1ThemeUrl: '',
};

export default function reducer(state = initialState, action: TActions) {
  let currentZoomLevel = 0;

  switch (action.type) {
    case Actions.FETCH_V1_THEME:
      return {
        ...state,
        v1ThemeUrl: action.url,
      };

    case Actions.FETCH_V1_THEME_PENDING:
      return {
        ...state,
        isLoaded: false,
        isLoading: true,
      };

    case Actions.FETCH_V1_THEME_DONE:
      return {
        ...state,
        currentCenter: action.theme.center,
        currentZoomLevel: action.theme.zoomLevel,
        dataSources: action.dataSources,
        isLoaded: true,
        isLoading: false,
        theme: action.theme,
        themeLayers: action.themeLayers,
        themeVisualizationLayers: action.themeVisualizationLayers,
      };

    case Actions.ZOOM_IN:
      currentZoomLevel = state.currentZoomLevel + 1;

      if (
        state.theme.maxZoomLevel &&
        currentZoomLevel > state.theme.maxZoomLevel
      ) {
        currentZoomLevel = state.theme.maxZoomLevel;
      }

      return {
        ...state,
        currentZoomLevel,
      };

    case Actions.ZOOM_OUT:
      currentZoomLevel = state.currentZoomLevel - 1;

      if (
        state.theme.minZoomLevel &&
        currentZoomLevel < state.theme.minZoomLevel
      ) {
        currentZoomLevel = state.theme.minZoomLevel;
      } else if (currentZoomLevel < 0) {
        currentZoomLevel = 0;
      }

      return {
        ...state,
        currentZoomLevel,
      };

    case Actions.SET_ZOOM:
      return {
        ...state,
        currentZoomLevel: action.zoomLevel,
      };

    case Actions.SET_CENTER:
      return {
        ...state,
        currentCenter: action.center,
      };

    case Actions.SET_CENTER_AND_ZOOM:
      return {
        ...state,
        currentCenter: action.center,
        currentZoomLevel: action.zoomLevel,
      };

    default:
      return state;
  }
}
