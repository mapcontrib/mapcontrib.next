import { convertV1Theme } from 'src/helpers/v1Converter';
import * as MC from 'src/types/mapcontrib';
import * as MC1 from 'src/types/mapcontribV1';

export enum Actions {
  FETCH_V1_THEME = 'theme/FETCH_V1_THEME',
  FETCH_V1_THEME_DONE = 'theme/FETCH_V1_THEME_DONE',
  FETCH_V1_THEME_FAILED = 'theme/FETCH_V1_THEME_FAILED',
  FETCH_V1_THEME_PENDING = 'theme/FETCH_V1_THEME_PENDING',
  ZOOM_IN = 'ZOOM_IN',
  ZOOM_OUT = 'ZOOM_OUT',
  SET_ZOOM = 'SET_ZOOM',
  SET_CENTER = 'SET_CENTER',
  SET_CENTER_AND_ZOOM = 'SET_CENTER_AND_ZOOM',
}

export interface IFetchV1Theme {
  type: Actions.FETCH_V1_THEME;
  url: string;
}
export const fetchV1Theme = (url: string): IFetchV1Theme => ({
  type: Actions.FETCH_V1_THEME,
  url: url.replace(/^\w+:\/\/.*\/bridge\//, ''),
});

export interface IFetchV1ThemePending {
  type: Actions.FETCH_V1_THEME_PENDING;
}
export const fetchV1ThemePending = (): IFetchV1ThemePending => ({
  type: Actions.FETCH_V1_THEME_PENDING,
});

export interface IFetchV1ThemeFailed {
  message: string;
  type: Actions.FETCH_V1_THEME_FAILED;
}
export const fetchV1ThemeFailed = (message: string): IFetchV1ThemeFailed => ({
  message,
  type: Actions.FETCH_V1_THEME_FAILED,
});

export interface IFetchV1ThemeDone {
  dataSources: MC.DataSource[];
  theme: MC.Theme;
  themeVisualizationLayers: MC.ThemeVisualizationLayer[];
  themeLayers: MC.ThemeLayer[];
  type: Actions.FETCH_V1_THEME_DONE;
}
export const fetchV1ThemeDone = (theme: MC1.Theme): IFetchV1ThemeDone => {
  const conversionResult = convertV1Theme(theme);

  return {
    dataSources: conversionResult.dataSources,
    theme: conversionResult.theme,
    themeLayers: conversionResult.themeLayers,
    themeVisualizationLayers: conversionResult.themeVisualizationLayers,
    type: Actions.FETCH_V1_THEME_DONE,
  };
};

export interface IZoomIn {
  type: Actions.ZOOM_IN;
}
export const zoomIn = (): IZoomIn => ({
  type: Actions.ZOOM_IN,
});

export interface IZoomOut {
  type: Actions.ZOOM_OUT;
}
export const zoomOut = (): IZoomOut => ({
  type: Actions.ZOOM_OUT,
});

export interface ISetZoom {
  type: Actions.SET_ZOOM;
  zoomLevel: number;
}
export const setZoom = (zoomLevel: number): ISetZoom => ({
  type: Actions.SET_ZOOM,
  zoomLevel,
});

export interface ISetCenter {
  center: MC.LatLng;
  type: Actions.SET_CENTER;
}
export const setCenter = (center: MC.LatLng): ISetCenter => ({
  center,
  type: Actions.SET_CENTER,
});

export interface ISetCenterAndZoom {
  center: MC.LatLng;
  type: Actions.SET_CENTER_AND_ZOOM;
  zoomLevel: number;
}
export const setCenterAndZoom = (
  center: MC.LatLng,
  zoomLevel: number,
): ISetCenterAndZoom => ({
  center,
  type: Actions.SET_CENTER_AND_ZOOM,
  zoomLevel,
});

export type TActions =
  | IFetchV1Theme
  | IFetchV1ThemePending
  | IFetchV1ThemeFailed
  | IFetchV1ThemeDone
  | IZoomIn
  | IZoomOut
  | ISetZoom
  | ISetCenter
  | ISetCenterAndZoom;
