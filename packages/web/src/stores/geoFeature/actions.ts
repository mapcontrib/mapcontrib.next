import * as MC from 'src/types/mapcontrib';

export enum Actions {
  ADD_FEATURES = 'geoFeature/ADD_FEATURES',
}

export interface IAddFeatures {
  type: Actions.ADD_FEATURES;
  dataSourceId: MC.Uuid;
  features: MC.OverpassFeature[];
}
export const addFeatures = (
  dataSourceId: MC.Uuid,
  features: MC.OverpassFeature[],
): IAddFeatures => ({
  dataSourceId,
  features,
  type: Actions.ADD_FEATURES,
});

export type TActions = IAddFeatures;
