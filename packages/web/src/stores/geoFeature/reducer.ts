import { computeOsmId } from 'src/helpers/osm.js';
import * as MC from 'src/types/mapcontrib';
import { Actions, TActions } from './actions';

export interface IGeoFeatureState {
  features: {
    [dataSourceId: string]: {
      [osmId: string]: MC.OverpassFeature;
    };
  };
}

const initialState: IGeoFeatureState = {
  features: {},
};

export default function reducer(state = initialState, action: TActions) {
  switch (action.type) {
    case Actions.ADD_FEATURES:
      const dataSourceId = action.dataSourceId.toString();
      const dataSourceFeatures = state.features[dataSourceId] || [];
      const newFeatures = {};

      action.features
        .filter(({ id, type }) => !dataSourceFeatures[computeOsmId(type, id)])
        .forEach(feature => {
          newFeatures[computeOsmId(feature.type, feature.id)] = feature;
        });

      return {
        ...state,
        features: {
          ...state.features,
          [dataSourceId]: { ...state.features[dataSourceId], ...newFeatures },
        },
      };

    default:
      return state;
  }
}
