import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import SettingsSidebar from 'src/components/settings/SettingsSidebar.jsx';
import LayersSidebar from 'src/components/settings/LayersSidebar.jsx';

const SettingsRoutes = ({ themePath }) => (
  <Fragment>
    <Route
      exact
      path="/t/:fragment/:title?/settings"
      // eslint-disable-next-line
      children={props => <SettingsSidebar themePath={themePath} {...props} />}
    />
    <Route
      exact
      path="/t/:fragment/:title?/settings/layers"
      // eslint-disable-next-line
      children={props => <LayersSidebar themePath={themePath} {...props} />}
    />
  </Fragment>
);

SettingsRoutes.propTypes = {
  themePath: PropTypes.string.isRequired,
  themeTitle: PropTypes.string.isRequired,
};

SettingsRoutes.defaultProps = {};

SettingsRoutes.displayName = 'SettingsRoutes';

export default SettingsRoutes;
