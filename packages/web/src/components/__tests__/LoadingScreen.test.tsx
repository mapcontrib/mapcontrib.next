import { shallow } from 'enzyme';
import * as React from 'react';

import LoadingScreen from '../LoadingScreen';

describe('LoadingScreen component', () => {
  it('Should render properly with default props', () => {
    const wrapper = shallow(<LoadingScreen />);
    expect(wrapper).toMatchSnapshot();
  });
});
