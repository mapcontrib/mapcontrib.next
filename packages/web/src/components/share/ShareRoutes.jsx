import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import ShareSidebar from 'src/components/share/ShareSidebar.jsx';

const ShareRoutes = ({ themePath }) => (
  <Fragment>
    <Route
      exact
      path="/t/:fragment/:title?/share"
      // eslint-disable-next-line
      children={props => <ShareSidebar themePath={themePath} {...props} />}
    />
  </Fragment>
);

ShareRoutes.propTypes = {
  themePath: PropTypes.string.isRequired,
};

ShareRoutes.defaultProps = {};

ShareRoutes.displayName = 'ShareRoutes';

export default ShareRoutes;
