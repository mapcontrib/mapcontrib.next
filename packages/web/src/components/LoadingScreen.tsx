import { Loader, WhiteTheme } from 'osm-ui-react';
import * as React from 'react';

const LoadingScreen = () => (
  <WhiteTheme>
    <Loader centered={true} />
  </WhiteTheme>
);

export default LoadingScreen;
