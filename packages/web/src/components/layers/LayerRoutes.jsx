import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import LayersSidebar from 'src/containers/layers/LayersSidebar.js';
import LayerEditionSidebar from 'src/containers/layers/LayerEditionSidebar.js';

const LayerRoutes = ({ themePath }) => (
  <Fragment>
    <Route
      exact
      path="/t/:fragment/:title?/layers"
      // eslint-disable-next-line
      children={props => <LayersSidebar themePath={themePath} {...props} />}
    />
    <Route
      exact
      path="/t/:fragment/:title?/layers/:id"
      // eslint-disable-next-line
      children={props => (
        <LayerEditionSidebar themePath={themePath} {...props} />
      )}
    />
  </Fragment>
);

LayerRoutes.propTypes = {
  themePath: PropTypes.string.isRequired,
};

LayerRoutes.defaultProps = {};

LayerRoutes.displayName = 'LayerRoutes';

export default LayerRoutes;
