import * as React from 'react';
import PropTypes from 'prop-types';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import styled from 'styled-components';
import { WhiteTheme, Section } from 'osm-ui-react';

import Theme from 'src/components/Theme.jsx';
import SettingsRoutes from 'src/components/settings/SettingsRoutes.jsx';
import MainRoutes from 'src/components/main/MainRoutes.jsx';
import UserRoutes from 'src/components/user/UserRoutes.jsx';
import ShareRoutes from 'src/components/share/ShareRoutes.jsx';
import LayerRoutes from 'src/components/layers/LayerRoutes.jsx';
import PointRoutes from 'src/components/points/PointRoutes.js';

import {
  getMinZoomFromTileConfigId,
  getMaxZoomFromTileConfigId,
} from 'src/helpers/map.js'; // FIXME - To remove

const StyledCanvas = styled(Section)`
  background-color: #ccc;
`;

class ThemeApp extends React.Component {
  UNSAFE_componentWillMount() {
    const tileConfigId = 'osmFr'; // FIXME - To remove
    this.props.setMapTileConfigId(tileConfigId); // FIXME - To remove
    this.props.setMapMinZoom(getMinZoomFromTileConfigId(tileConfigId)); // FIXME - To remove
    this.props.setMapMaxZoom(getMaxZoomFromTileConfigId(tileConfigId)); // FIXME - To remove
  }

  render() {
    const { themePath, themeTitle, setFragment } = this.props;

    return (
      <Router>
        <WhiteTheme>
          <StyledCanvas appCanvas>
            <Switch>
              <Route
                path="/t/:fragment/:title?"
                render={props => (
                  <Theme
                    themePath={themePath}
                    setFragment={setFragment}
                    {...props}
                  />
                )}
              />
              <Route
                path="/"
                render={() => <Redirect to="/t/cl7syt/MapContrib" />}
              />
            </Switch>
            <SettingsRoutes themePath={themePath} themeTitle={themeTitle} />
            <MainRoutes themePath={themePath} themeTitle={themeTitle} />
            <UserRoutes themePath={themePath} />
            <ShareRoutes themePath={themePath} />
            <LayerRoutes themePath={themePath} />
            <PointRoutes themePath={themePath} />
          </StyledCanvas>
        </WhiteTheme>
      </Router>
    );
  }
}

ThemeApp.propTypes = {
  setMapTileConfigId: PropTypes.func.isRequired,
  setMapMinZoom: PropTypes.func.isRequired,
  setMapMaxZoom: PropTypes.func.isRequired,
  setFragment: PropTypes.func.isRequired,
};

ThemeApp.defaultProps = {};

ThemeApp.displayName = 'ThemeApp';

export default ThemeApp;
