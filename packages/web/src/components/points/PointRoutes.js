import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import OsmoseSidebar from 'src/containers/points/OsmoseSidebar.js';

const PointRoutes = ({ themePath }) => (
  <Fragment>
    <Route
      exact
      path="/t/:fragment/:title?/points/osmose/:id"
      // eslint-disable-next-line
      children={props => <OsmoseSidebar themePath={themePath} {...props} />}
    />
  </Fragment>
);

PointRoutes.propTypes = {
  themePath: PropTypes.string.isRequired,
};

PointRoutes.defaultProps = {};

PointRoutes.displayName = 'PointRoutes';

export default PointRoutes;
