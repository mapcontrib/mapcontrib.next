import { flatten as _flatten, values as _values } from 'lodash';
import { createSelector } from 'reselect';

import { buildOffMapMarker } from 'src/helpers/ui';
import { IBridgeState } from 'src/stores/bridge/reducer';
import { IGeoFeatureState } from 'src/stores/geoFeature/reducer';
import { IThemeState } from 'src/stores/theme/reducer';
import * as MC from 'src/types/mapcontrib';

export const dataSourcesSelector = ({ theme }: { theme: IThemeState }) =>
  theme.dataSources;

export const themeLayersSelector = ({ theme }: { theme: IThemeState }) =>
  theme.themeLayers;

export const themeVisualizationLayersSelector = ({
  theme,
}: {
  theme: IThemeState;
}) => theme.themeVisualizationLayers;

export const featuresSelector = ({
  geoFeature,
}: {
  geoFeature: IGeoFeatureState;
}) => geoFeature.features;

export const mergeMarkerClustersSelector = ({
  bridge,
}: {
  bridge: IBridgeState;
}) => bridge.mergeMarkerClusters;

export const augmentedFeatureListSelector = createSelector(
  featuresSelector,
  dataSourcesSelector,
  themeLayersSelector,
  (features, dataSources, themeLayers) => {
    const augmentedFeatureList: MC.AugmentedFeature[] = [];
    const offMapMarkers = {};

    themeLayers.forEach(themeLayer => {
      offMapMarkers[themeLayer.uuid] = buildOffMapMarker(themeLayer);
    });

    dataSources.forEach(dataSource => {
      const dataSourceFeatures = _values(features[dataSource.uuid]);
      const layer = themeLayers.find(l => l.uuid === dataSource.layerId);

      if (!layer) {
        return;
      }

      for (const feature of dataSourceFeatures) {
        const augmentedFeature = {
          dataSource,
          feature,
          layer,
          offMapMarker: offMapMarkers[dataSource.layerId],
        };

        augmentedFeatureList.push(augmentedFeature);
      }
    });

    return augmentedFeatureList;
  },
);

export const featureCountPerLayerSelector = createSelector(
  featuresSelector,
  dataSourcesSelector,
  (features, dataSources) => {
    const featureCount: { [layerId: string]: number } = {};

    dataSources.forEach(dataSource => {
      if (!featureCount[dataSource.layerId]) {
        featureCount[dataSource.layerId] = 0;
      }

      if (features[dataSource.uuid]) {
        featureCount[dataSource.layerId] += Object.keys(
          features[dataSource.uuid],
        ).length;
      }
    });

    return featureCount;
  },
);

interface IMergeClustersIfNeededSelector {
  themeLayers: MC.ThemeLayer[];
  themeVisualizationLayers: MC.ThemeVisualizationLayer[];
}
// If the user wants to use the maker clusters for all the layers, mergeMarkerClusters is true
// In that case, we keep only the first cluster visualization layer and use it for all the layers
export const mergeClustersIfNeededSelector = createSelector(
  themeLayersSelector,
  themeVisualizationLayersSelector,
  mergeMarkerClustersSelector,
  (
    themeLayers,
    themeVisualizationLayers,
    mergeMarkerClusters,
  ): IMergeClustersIfNeededSelector => {
    if (!mergeMarkerClusters) {
      return { themeLayers, themeVisualizationLayers };
    }

    const firstClusterLayer = themeVisualizationLayers.find(
      layer => layer.type === MC.DataVisualizationType.CLUSTER,
    );

    if (!firstClusterLayer) {
      return { themeLayers, themeVisualizationLayers };
    }

    const result: IMergeClustersIfNeededSelector = {
      themeLayers: [],
      themeVisualizationLayers: [firstClusterLayer],
    };

    themeVisualizationLayers.forEach(visualizationLayer => {
      if (visualizationLayer.type !== MC.DataVisualizationType.CLUSTER) {
        result.themeVisualizationLayers.push(visualizationLayer);

        result.themeLayers.push(
          ...themeLayers.filter(
            layer => layer.visualizationLayerId === visualizationLayer.uuid,
          ),
        );
      } else {
        const layers = themeLayers
          .filter(
            layer => layer.visualizationLayerId === visualizationLayer.uuid,
          )
          .map(layer => ({
            ...layer,
            visualizationLayerId: firstClusterLayer.uuid,
          }));

        result.themeLayers.push(...layers);
      }
    });

    return result;
  },
);
