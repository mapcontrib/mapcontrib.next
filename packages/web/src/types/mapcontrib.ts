// tslint:disable:interface-name

export type Uuid = string;
export type Fragment = string;
export type OsmUserId = string;
export type OsmId = string;
export type OsmType = 'node' | 'way' | 'relation';
export type OsmVersion = string;

export interface OverpassFeature {
  id: OsmId;
  lat: number;
  lon: number;
  type: OsmType;
  tags: {
    [key: string]: string;
  };
}

export interface AugmentedFeature {
  dataSource: DataSource;
  feature: OverpassFeature;
  layer: ThemeLayer;
  offMapMarker: any;
}

export interface HSLColor {
  h: number;
  s: number;
  l: number;
}

export interface LatLng {
  lat: number;
  lng: number;
}

export interface IBounds {
  _northEast: LatLng;
  _southWest: LatLng;
}

export enum DataSourceType {
  CSV = 'CSV',
  GEOJSON = 'GEOJSON',
  GPX = 'GPX',
  KML = 'KML',
  OSMOSE = 'OSMOSE',
  OVERPASS = 'OVERPASS',
  POLYLINE = 'POLYLINE',
  TOPOJSON = 'TOPOJSON',
  WKT = 'WKT',
}

export enum DataVisualizationType {
  BASIC = 'BASIC',
  CLUSTER = 'CLUSTER',
  HEAT = 'HEAT',
}

export enum OsmTagType {
  CHECK = 'check',
  COMBO = 'combo',
  DEFAULT_CHECK = 'defaultCheck',
  EMAIL = 'email',
  MULTI_COMBO = 'multiCombo',
  NUMBER = 'number',
  TEL = 'tel',
  TEXT = 'text',
  TEXTAREA = 'textarea',
  TYPE_COMBO = 'typeCombo',
  URL = 'url',
}

export enum NonOsmTagType {
  FILE = 'file',
}

export type TagType = OsmTagType & NonOsmTagType;

export enum Geocoder {
  NOMINATIM = 'NOMINATIM',
  PHOTON = 'PHOTON',
}

export enum FeatureInformationDisplayType {
  MODAL = 'MODAL',
  POPUP = 'POPUP',
  SIDEBAR = 'SIDEBAR',
}

export enum MarkerShape {
  BASIC_CIRCLE = 'basicCircle',
  BASIC_DIAMOND = 'basicDiamond',
  BASIC_DOWN_TRIANGLE = 'basicDownTriangle',
  BASIC_LEFT_TRIANGLE = 'basicLeftTriangle',
  BASIC_RIGHT_TRIANGLE = 'basicRightTriangle',
  BASIC_SQUARE = 'basicSquare',
  BASIC_UP_TRIANGLE = 'basicUpTriangle',
  POINTER_CIRCLE_PIN = 'pointerCirclePin',
  POINTER_CLASSIC = 'pointerClassic',
  POINTER_CLASSIC_THIN = 'pointerClassicThin',
}
export enum MarkerIconType {
  EXTERNAL = 'EXTERNAL',
  LIBRARY = 'LIBRARY',
}

export interface Theme {
  center: LatLng;
  creationDate: Date;
  description: string;
  featureInfoDisplay: FeatureInformationDisplayType;
  fragment: Fragment;
  geocoder: Geocoder;
  id: string;
  isAutoCentered: boolean;
  maxZoomLevel: number;
  minZoomLevel: number;
  modificationDate: Date;
  movementRadius?: number;
  name: string;
  tiles: TileId[];
  uuid: Uuid;
  zoomLevel: number;
}

export interface ThemeLayer {
  creationDate: Date;
  description: string;
  featureInfoTemplate: string;
  id: string;
  isVisible: boolean;
  visualizationLayerId?: Uuid;
  markerColor: HSLColor;
  markerIcon: string;
  markerIconType: MarkerIconType;
  markerIconUrl?: string;
  markerShape: MarkerShape;
  modificationDate: Date;
  name: string;
  order: number;
  themeId: Uuid;
  uuid: Uuid;
}

export interface ThemeVisualizationLayerBase {
  id: string;
  themeId: Uuid;
  uuid: Uuid;
}

export interface ThemeVisualizationLayerBasic
  extends ThemeVisualizationLayerBase {
  type: DataVisualizationType.BASIC;
}

export interface ThemeVisualizationLayerCluster
  extends ThemeVisualizationLayerBase {
  type: DataVisualizationType.CLUSTER;
}

export interface ThemeVisualizationLayerHeat
  extends ThemeVisualizationLayerBase {
  heatBlur: number;
  heatMax: number;
  heatMaxZoom: number;
  heatMinOpacity: number;
  heatRadius: number;
  type: DataVisualizationType.HEAT;
}

export type ThemeVisualizationLayer =
  | ThemeVisualizationLayerBasic
  | ThemeVisualizationLayerCluster
  | ThemeVisualizationLayerHeat;

export interface DataSourceBase {
  hasCache: boolean;
  id: string;
  layerId: Uuid;
  themeId: Uuid;
  uuid: Uuid;
}

export interface DataSourceOverpass extends DataSourceBase {
  // cacheArchive: boolean;
  // cacheBounds: Bounds;
  // cacheDeletedFeatures: OsmCacheFeature[];
  // cachefileUri?: string;
  // cacheUpdateDate: Date;
  // cacheUpdateError: string | null;
  // cacheUpdateSuccess: boolean;
  // cacheUpdateSuccessDate: Date;
  minZoom: number;
  query: string;
  type: DataSourceType.OVERPASS;
}

export interface DataSourceOsmose extends DataSourceBase {
  minZoom: number;
  query: string;
  type: DataSourceType.OSMOSE;
}

export interface DataSourceOther extends DataSourceBase {
  type:
    | DataSourceType.CSV
    | DataSourceType.GEOJSON
    | DataSourceType.GPX
    | DataSourceType.KML
    | DataSourceType.POLYLINE
    | DataSourceType.TOPOJSON
    | DataSourceType.WKT;
}

export type DataSource =
  | DataSourceOverpass
  | DataSourceOsmose
  | DataSourceOther;

export type TileId = string;

export interface ITile {
  id: TileId;
}
