declare module 'enzyme';
declare module 'leaflet-nectarivore';
declare module 'osm-ui-react';
declare module 'react-leaflet';
declare module 'react-leaflet-markercluster';
declare module '*.svg';
declare module '*.png';
declare module '*.jpg';

declare module '*.json' {
  const value: any;
  export default value;
}
