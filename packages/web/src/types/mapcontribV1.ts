// tslint:disable:interface-name

export enum ColorName {
  white = 'white',
  black = 'black',
  yellow = 'yellow',
  lightYellow = 'lightYellow',
  darkYellow = 'darkYellow',
  orange = 'orange',
  lightOrange = 'lightOrange',
  darkOrange = 'darkOrange',
  red = 'red',
  lightRed = 'lightRed',
  darkRed = 'darkRed',
  brown = 'brown',
  lightBrown = 'lightBrown',
  darkBrown = 'darkBrown',
  purple = 'purple',
  lightPurple = 'lightPurple',
  darkPurple = 'darkPurple',
  blue = 'blue',
  lightBlue = 'lightBlue',
  darkBlue = 'darkBlue',
  turquoise = 'turquoise',
  lightTurquoise = 'lightTurquoise',
  darkTurquoise = 'darkTurquoise',
  green = 'green',
  lightGreen = 'lightGreen',
  darkGreen = 'darkGreen',
  gray = 'gray',
  lightGray = 'lightGray',
  darkGray = 'darkGray',
  anthracite = 'anthracite',
  lightAnthracite = 'lightAnthracite',
  darkAnthracite = 'darkAnthracite',
}

export type Color =
  | ColorName
  | {
      h: number;
      s: number;
      l: number;
    };

export enum TileId {
  cadastre = 'cadastre',
  hikeBikeMap = 'hikeBikeMap',
  hot = 'hot',
  hydda = 'hydda',
  landscape = 'landscape',
  mapboxStreetsSatellite = 'mapboxStreetsSatellite',
  openCycleMap = 'openCycleMap',
  openRiverboatMap = 'openRiverboatMap',
  openTopoMap = 'openTopoMap',
  osm = 'osm',
  osmDe = 'osmDe',
  osmFr = 'osmFr',
  osmFrBano = 'osmFrBano',
  osmMonochrome = 'osmMonochrome',
  osmOutdoors = 'osmOutdoors',
  osmRoads = 'osmRoads',
  toner = 'toner',
  transport = 'transport',
  watercolor = 'watercolor',
}

export interface Tile {
  attribution: string;
  maxZoom: number;
  minZoom: number;
  name: string;
  urlTemplate: string[];
}

export type TileList = { [P in TileId]: Tile };

export enum LayerTypeId {
  csv = 'csv',
  geojson = 'geojson',
  gpx = 'gpx',
  osmose = 'osmose',
  overpass = 'overpass',
}

export enum RootLayerType {
  heat = 'heat',
  markerCluster = 'markerCluster',
}

export enum TagType {
  check = 'check',
  combo = 'combo',
  defaultCheck = 'defaultCheck',
  email = 'email',
  file = 'file',
  multiCombo = 'multiCombo',
  number = 'number',
  tel = 'tel',
  text = 'text',
  textarea = 'textarea',
  typeCombo = 'typeCombo',
  url = 'url',
}

export enum Geocoder {
  nominatim = 'nominatim',
  photon = 'photon',
}

export enum InfoDisplay {
  column = 'column',
  modal = 'modal',
  popup = 'popup',
}

export enum OverPassCacheError {
  badRequest = 'badRequest',
  memory = 'memory',
  timeout = 'timeout',
  unknown = 'unknown',
}

export enum MarkerShapeId {
  marker1 = 'marker1',
  marker2 = 'marker2',
  marker3 = 'marker3',
}

export enum MarkerIconType {
  external = 2,
  library = 1,
}

export type Uuid = string;
export type Fragment = string;
export type ISOStringDate = string;
export type MongoDBId = string;
export type LocaleId = string;
export type OsmUserId = string;
export type OsmId = string;
export type OsmType = string;
export type OsmVersion = string;
export type MovementRadius = string | number | null;

export interface LatLng {
  lat: number;
  lng: number;
}

export interface Bounds {
  _northEast: LatLng;
  _southWest: LatLng;
}

export type Locale = {
  [key: string]: string;
} & {
  options: {
    [key: string]: string;
  };
};

export interface Locales {
  [key: string]: Locale;
}

export interface Layer {
  _id: MongoDBId;
  cache: boolean;
  cacheArchive: boolean;
  cacheBounds: Bounds;
  cacheDeletedFeatures: OsmCacheFeature[];
  cacheUpdateDate: ISOStringDate;
  cacheUpdateError: string | null;
  cacheUpdateSuccess: boolean;
  cacheUpdateSuccessDate: ISOStringDate;
  color: Color;
  creationDate: ISOStringDate;
  dataEditable: boolean;
  description: string;
  fileUri?: string;
  heatBlur: number;
  heatMax: number;
  heatMaxZoom: number;
  heatMinOpacity: number;
  heatRadius: number;
  locales: Locales;
  markerColor: Color;
  markerIcon: string;
  markerIconType: MarkerIconType;
  markerIconUrl: string | null;
  markerShape: MarkerShapeId;
  minZoom: number;
  modificationDate: ISOStringDate;
  name: string;
  order: number;
  overpassRequest: string;
  popupContent: string;
  rootLayerType: RootLayerType;
  type: LayerTypeId;
  uniqid: Uuid;
  uuid: Uuid;
  visible: boolean;
}

export interface PresetCategory {
  _id: MongoDBId;
  creationDate: ISOStringDate;
  locales: Locales;
  modificationDate: ISOStringDate;
  name: string;
  parentUuid: Uuid;
  uuid: Uuid;
}

export interface PresetTag {
  keyReadOnly: boolean;
  nonOsmData: boolean;
  tag: TagType;
  value: string;
  valueReadOnly: boolean;
}

export interface Preset {
  _id: MongoDBId;
  creationDate: ISOStringDate;
  description: string;
  locales: Locales;
  modificationDate: ISOStringDate;
  name: string;
  order: number;
  parentUuid: Uuid;
  tags: PresetTag[];
  uuid: Uuid;
}

export interface Tag {
  _id: MongoDBId;
  creationDate: ISOStringDate;
  key: string;
  locales: Locales;
  modificationDate: ISOStringDate;
  name: string;
  options: string[];
  order: number;
  type: TagType;
  uuid: Uuid;
}

export interface Theme {
  _id: MongoDBId;
  analyticScript: string;
  autoCenter: boolean;
  center: LatLng;
  color: ColorName | string;
  creationDate: ISOStringDate;
  description: string;
  fragment: Fragment;
  geocoder: Geocoder;
  infoDisplay: InfoDisplay;
  layers: Layer[];
  locales: Locales;
  maxZoomLevel?: string | number;
  minZoomLevel?: string | number;
  modificationDate: ISOStringDate;
  movementRadius?: MovementRadius;
  name: string;
  osmOwners: OsmUserId[];
  owners: MongoDBId[];
  presetCategories: PresetCategory[];
  presets: Preset[];
  tags: Tag[];
  tiles: TileId[];
  userId: string;
  zoomLevel: number;
}

export interface NonOsmData {
  creationDate: ISOStringDate;
  modificationDate: ISOStringDate;
  osmId: OsmId;
  osmType: OsmType;
  tags: Array<{
    key: string;
    value: string;
    type: 'text' | 'file';
  }>;
  themeFragment: Fragment;
  userId: MongoDBId;
}

export interface OsmCacheFeature {
  _id: MongoDBId;
  creationDate: ISOStringDate;
  modificationDate: ISOStringDate;
  osmElement: {};
  osmId: OsmId;
  osmType: OsmType;
  osmVersion: OsmVersion;
  overPassElement: {};
  themeFragment: Fragment;
  userId: MongoDBId;
}

export interface Owner {
  _id: MongoDBId;
  avatar: string;
  displayName: string;
  osmId: OsmId;
}

export interface User {
  _id: MongoDBId;
  avatar: string;
  creationDate: ISOStringDate;
  displayName: string;
  favoriteThemes: Fragment[];
  modificationDate: ISOStringDate;
  osmId: OsmId;
  token: string;
  tokenSecret: string;
}

export interface UserTheme {
  color: ColorName;
  fragment: Fragment;
  name: string;
}

export type UserFavoriteThemesData = UserTheme;
