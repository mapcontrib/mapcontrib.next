export const sourceTypes = Object.freeze({
  OSMOSE: 'OSMOSE',
  OVERPASS: 'OVERPASS',
});
