export const CHANGESET_CREATED_BY = 'MapContrib {version}';
export const CHANGESET_COMMENT =
  'Contribution sent from the following MapContrib theme: {url}';
