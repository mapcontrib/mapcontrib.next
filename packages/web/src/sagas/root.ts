import { fork } from 'redux-saga/effects';

import themeV1Saga from 'src/sagas/themeV1';

function* saga() {
  yield [fork(themeV1Saga)];
}

export default saga;
