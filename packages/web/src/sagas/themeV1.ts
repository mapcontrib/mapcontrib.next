import { call, put, takeLatest } from 'redux-saga/effects';

import { buildApiUrlFromV1ThemeUrl } from 'src/helpers/v1Converter';
import {
  Actions,
  fetchV1ThemeDone,
  fetchV1ThemeFailed,
  fetchV1ThemePending,
  IFetchV1Theme,
} from 'src/stores/theme/actions';

function* fetchV1ThemeSaga({ url }: IFetchV1Theme) {
  try {
    yield put(fetchV1ThemePending());

    const theme = yield call(fetchV1Theme, url);

    yield put(fetchV1ThemeDone(theme));
  } catch (message) {
    yield put(fetchV1ThemeFailed(message));
  }
}

function fetchV1Theme(url: string) {
  return fetch(buildApiUrlFromV1ThemeUrl(url)).then(res => res.json());
}

export default function*() {
  yield takeLatest(Actions.FETCH_V1_THEME as any, fetchV1ThemeSaga);
}
