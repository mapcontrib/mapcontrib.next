import { connect } from 'react-redux';
import MapComponent from 'src/components/Map.jsx';
import { setMapZoom } from 'src/actions/map.js';
import { findTileSourcesFromConfigId } from 'src/helpers/map.js';

const mapStateToProps = (state, { match, history }) => ({
  zoom: state.map.zoom,
  minZoom: state.map.minZoom,
  maxZoom: state.map.maxZoom,
  tileSources: findTileSourcesFromConfigId(state.map.tileConfigId),
  layers: Object.values(state.layers).filter(layer => layer.isVisible),
  sources: state.sources,
  submittedErrors: state.osmose.submitted,
  openOsmose: id => history.push(`${match.url}/points/osmose/${id}`),
});

const mapDispatchToProps = {
  setMapZoom,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MapComponent);
