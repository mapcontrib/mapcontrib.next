import { connect } from 'react-redux';
import { addSubmitted } from 'src/actions/osmose.js';
import OsmoseSidebar from 'src/components/points/OsmoseSidebar.jsx';

const mapDispatchToProps = {
  addSubmitted,
};

export default connect(
  null,
  mapDispatchToProps,
)(OsmoseSidebar);
