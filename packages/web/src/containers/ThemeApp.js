import { connect } from 'react-redux';

import ThemeApp from 'src/components/ThemeApp.jsx';
import {
  setMapTileConfigId,
  setMapMinZoom,
  setMapMaxZoom,
} from 'src/actions/map.js'; // FIXME - To remove
import { setFragment } from 'src/actions/theme.js';

const mapStateToProps = state => ({
  themeTitle: state.theme.title,
  themePath: state.theme.path,
});

const mapDispatchToProps = dispatch => ({
  setMapTileConfigId: configId => dispatch(setMapTileConfigId(configId)), // FIXME - To remove
  setMapMinZoom: zoom => dispatch(setMapMinZoom(zoom)), // FIXME - To remove
  setMapMaxZoom: zoom => dispatch(setMapMaxZoom(zoom)), // FIXME - To remove
  setFragment: fragment => dispatch(setFragment(fragment)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ThemeApp);
