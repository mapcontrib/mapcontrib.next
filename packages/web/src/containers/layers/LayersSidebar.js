import { connect } from 'react-redux';
import { addLayer } from 'src/actions/layers.js';

import LayersSidebar from 'src/components/layers/LayersSidebar.jsx';

const mapStateToProps = ({ layers }) => ({
  layers: Object.values(layers),
});

const mapDispatchToProps = {
  addLayer,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LayersSidebar);
