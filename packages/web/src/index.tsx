import { Router } from '@reach/router';
import 'osm-ui-react/dist/index.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Bridge from 'src/apps/bridge/Loadable';
import Theme from 'src/apps/theme/Loadable';
import 'src/index.css';
import registerServiceWorker from 'src/registerServiceWorker';

ReactDOM.render(
  <Router>
    <Bridge path="bridge/*" />
    <Theme path="t/*" />
  </Router>,
  document.getElementById('root') as HTMLElement,
);
registerServiceWorker();
