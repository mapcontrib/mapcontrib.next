import { RouteComponentProps } from '@reach/router';
import * as React from 'react';
import { Provider } from 'react-redux';

import ThemeApp from 'src/containers/ThemeApp.js';
import store from 'src/store.js';

export default class App extends React.Component<RouteComponentProps> {
  public render() {
    return (
      <Provider store={store}>
        <ThemeApp />
      </Provider>
    );
  }
}
