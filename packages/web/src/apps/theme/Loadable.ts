import * as Loadable from 'react-loadable';

import LoadingScreen from 'src/components/LoadingScreen';

const Bridge = Loadable({
  loader: () => import('src/apps/theme/App'),
  loading: LoadingScreen,
});

export default Bridge;
