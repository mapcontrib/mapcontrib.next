import { RouteComponentProps } from '@reach/router';
import * as React from 'react';
import { Provider } from 'react-redux';

import ThemeContainer from 'src/apps/bridge/containers/Theme';
import store from 'src/apps/bridge/store';
import { fetchV1Theme } from 'src/stores/theme/actions';

export default class App extends React.Component<RouteComponentProps> {
  public componentWillMount() {
    store.dispatch(fetchV1Theme(window.location.href));
  }

  public render() {
    return (
      <Provider store={store}>
        <ThemeContainer />
      </Provider>
    );
  }
}
