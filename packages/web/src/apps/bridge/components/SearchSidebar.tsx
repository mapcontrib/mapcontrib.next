import { flatten as _flatten, values as _values } from 'lodash';
import { Form, Sidebar } from 'osm-ui-react';
import * as React from 'react';
import styled from 'styled-components';

import SearchItem from 'src/apps/bridge/components/SearchItem';
import * as MC from 'src/types/mapcontrib';

const StyledForm = styled(Form)`
  margin-bottom: 2.5rem;
`;

const FilteredFeaturesCount = styled.div`
  text-align: right;
  font-style: italic;
  color: #888;
`;

interface IProps {
  augmentedFeatures: MC.AugmentedFeature[];
  close: () => void;
  mapContains: (latLng: MC.LatLng) => boolean;
  opened: boolean;
  setCenterAndZoom: (center: MC.LatLng, zoomLevel: number) => void;
}

interface IState {
  filteredAugmentedFeatures: MC.AugmentedFeature[];
  inBbox: boolean;
  inputValue: string;
}

export default class SearchSidebar extends React.PureComponent<IProps, IState> {
  public static getDerivedStateFromProps(props: IProps, state: IState) {
    const { mapContains } = props;

    const filteredAugmentedFeatures = props.augmentedFeatures.filter(
      augmentedFeature => {
        const { feature } = augmentedFeature;
        const { lat, lon: lng } = feature;

        if (!feature.tags || !feature.tags.name || feature.type !== 'node') {
          return false;
        }

        if (state.inBbox && (!lat || !lng || !mapContains({ lat, lng }))) {
          return false;
        }

        return !!feature.tags.name.match(new RegExp(state.inputValue, 'i'));
      },
    );

    return {
      ...state,
      filteredAugmentedFeatures,
    };
  }

  public state: IState = {
    filteredAugmentedFeatures: [],
    inBbox: true,
    inputValue: '',
  };

  public render() {
    const { close, opened } = this.props;
    const { filteredAugmentedFeatures, inBbox, inputValue } = this.state;

    return (
      <Sidebar opened={opened} onClose={close}>
        <Sidebar.Title>Search</Sidebar.Title>
        <StyledForm>
          <Form.Group>
            <Form.Input
              type="text"
              onChange={this.onInputChange}
              value={inputValue}
              placeholder="Filter..."
            />
          </Form.Group>
          <Form.Group>
            <Form.Checkbox
              id="only-visible"
              value="1"
              checked={inBbox}
              label="Only what is visible on the map"
              onChange={this.onInBboxChange}
            />
          </Form.Group>
        </StyledForm>

        {this.renderFeaturesCount(filteredAugmentedFeatures)}

        <section>{this.renderFeatures()}</section>
      </Sidebar>
    );
  }

  private onInputChange = (event: any) => {
    this.setState({
      ...this.state,
      inputValue: event.target.value,
    });
  };

  private onInBboxChange = (event: any) => {
    this.setState({
      ...this.state,
      inBbox: event.target.checked,
    });
  };

  private renderFeaturesCount = (
    filteredAugmentedFeatures: MC.AugmentedFeature[],
  ) => {
    const filteredFeaturesCount = filteredAugmentedFeatures.length;

    if (filteredFeaturesCount < 0) {
      return null;
    }

    return (
      <FilteredFeaturesCount>
        {filteredFeaturesCount}{' '}
        {filteredFeaturesCount > 1 ? 'results' : 'result'}
      </FilteredFeaturesCount>
    );
  };

  private renderFeatures = () => {
    return this.state.filteredAugmentedFeatures.map(
      (augmentedFeature, index: number) => (
        <SearchItem
          key={index}
          augmentedFeature={augmentedFeature}
          onClick={this.onClickFeature}
        />
      ),
    );
  };

  private onClickFeature = (augmentedFeature: MC.AugmentedFeature) => {
    const {
      feature: { lat, lon: lng },
    } = augmentedFeature;

    this.props.setCenterAndZoom({ lat, lng }, 18);
  };
}
