import { values as _values } from 'lodash';
import { Map } from 'osm-ui-react';
import * as React from 'react';

import Nectarivore from 'src/apps/bridge/components/Nectarivore';
import { buildMarkerColor, buildMarkerIcon } from 'src/helpers/ui';
import * as MC from 'src/types/mapcontrib';

interface IProps {
  addFeatures: (dataSourceId: MC.Uuid, features: MC.OverpassFeature[]) => void;
  dataSources: MC.DataSource[];
  features: {
    [dataSourceId: string]: {
      [osmId: string]: MC.OverpassFeature;
    };
  };
  themeVisualizationLayers: MC.ThemeVisualizationLayer[];
  themeLayers: MC.ThemeLayer[];
}

interface IState {
  geoFeatures: {
    [key: string]: any;
  };
}

export default class Layers extends React.PureComponent<IProps, IState> {
  public render() {
    return (
      <>
        {this.renderNectarivoreLayers()}
        {this.renderVisualizationLayers()}
      </>
    );
  }

  private renderNectarivoreLayers = () => {
    return this.props.dataSources
      .filter(dataSource => dataSource.type === MC.DataSourceType.OVERPASS)
      .map((dataSource, index) => (
        <Nectarivore
          key={index}
          addFeatures={this.addFeatures}
          dataSource={dataSource}
        />
      ));
  };

  private renderVisualizationLayers = () => {
    return this.props.themeVisualizationLayers.map(
      this.renderVisualizationLayer,
    );
  };

  private renderVisualizationLayer = (
    visualizationLayer: MC.ThemeVisualizationLayer,
    index: number,
  ) => {
    const layers = this.props.themeLayers.filter(
      layer => layer.visualizationLayerId === visualizationLayer.uuid,
    );

    switch (visualizationLayer.type) {
      case MC.DataVisualizationType.CLUSTER:
        return (
          <Map.MarkerClusterGroup
            key={index}
            color={buildMarkerColor(layers[0])}
            spiderfyOnMaxZoom={false}
            disableClusteringAtZoom={19}
          >
            {layers.map(this.renderLayer)}
          </Map.MarkerClusterGroup>
        );

      default:
        return (
          <Map.LayerGroup key={index}>
            {layers.map(this.renderLayer)}
          </Map.LayerGroup>
        );
    }
  };

  private renderLayer = (layer: MC.ThemeLayer, index: number) => {
    const dataSources = this.props.dataSources
      .filter(dataSource => dataSource.layerId === layer.uuid)
      .filter(dataSource => dataSource.type === MC.DataSourceType.OVERPASS);

    return dataSources.map(this.renderFeatures.bind(this, layer));
  };

  private renderFeatures = (
    layer: MC.ThemeLayer,
    dataSource: MC.DataSource,
  ) => {
    const features = _values(this.props.features[dataSource.uuid]);

    return features
      .filter(feature => !!feature.lat && !!feature.lon) // FIXME
      .map((feature: MC.OverpassFeature, index: number) => (
        <Map.Marker
          key={index}
          position={[feature.lat, feature.lon]}
          color={buildMarkerColor(layer)}
          icon={buildMarkerIcon(layer)}
          shape={layer.markerShape}
        />
      ));
  };

  private addFeatures = (
    dataSourceId: MC.Uuid,
    features: MC.OverpassFeature[],
  ) => {
    this.props.addFeatures(dataSourceId, features);
  };
}
