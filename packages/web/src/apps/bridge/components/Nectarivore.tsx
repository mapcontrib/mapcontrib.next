import LeafletNectarivore from 'leaflet-nectarivore';
import { Map } from 'osm-ui-react';
import { withLeaflet } from 'react-leaflet';

import * as MC from 'src/types/mapcontrib';

interface IState {
  features: {
    [key: string]: any;
  };
}

interface IProps {
  addFeatures: (dataSourceId: MC.Uuid, features: MC.OverpassFeature[]) => void;
  dataSource: MC.DataSource;
}

class Nectarivore extends Map.MapLayer<IProps, IState> {
  public createLeafletElement({ addFeatures, dataSource }: IProps) {
    switch (dataSource.type) {
      case MC.DataSourceType.OVERPASS:
        const { minZoom, query } = dataSource;

        return LeafletNectarivore.overpass({
          minZoom,
          onSuccess: (data: any) => {
            addFeatures(dataSource.uuid, data.elements);
          },
          query,
        });
    }
  }
}

export default withLeaflet(Nectarivore);
