import { shallow } from 'enzyme';
import * as React from 'react';

import Theme from '../Theme';

describe('Bridge Theme component', () => {
  it('Should render properly with default props', () => {
    const wrapper = shallow(
      <Theme
        currentCenter={{ lat: 12.3, lng: -34.5 }}
        currentZoomLevel={14}
        description="MapContrib"
        maxZoomLevel={22}
        minZoomLevel={0}
        name="MapContrib"
        setCenter={jest.fn()}
        setZoom={jest.fn()}
        v1ThemeUrl=""
        zoomIn={jest.fn()}
        zoomOut={jest.fn()}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
