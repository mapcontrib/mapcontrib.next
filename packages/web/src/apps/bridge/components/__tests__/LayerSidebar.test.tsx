import { shallow } from 'enzyme';
import * as React from 'react';

import * as MC from 'src/types/mapcontrib';
import LayersSidebar from '../LayersSidebar';

describe('Bridge LayersSidebar component', () => {
  it('Should render properly with default props', () => {
    const layer = {
      color: '',
      creationDate: new Date('2018/10/08'),
      description: '',
      featureInfoTemplate: '',
      id: '',
      isVisible: true,
      markerColor: { h: 0, s: 0, l: 0 },
      markerIcon: '',
      markerIconType: MC.MarkerIconType.LIBRARY,
      markerShape: MC.MarkerShape.POINTER_CIRCLE_PIN,
      modificationDate: new Date('2018/10/08'),
      name: 'Layer name',
      order: 0,
      themeId: 'xdfgdfghdfhgh-dfghfghfjghdfg-fghdfhgfghdfgh',
      uuid: 'qdfqkdjgqsdf-qsdfjgqsdfgqsfd-qsdfhgqsdf',
    };

    const wrapper = shallow(
      <LayersSidebar
        close={jest.fn()}
        featureCount={{ [layer.uuid]: 347 }}
        mergeMarkerClusters={true}
        opened={true}
        setMarkerClustersMerge={jest.fn()}
        themeLayers={[layer]}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
