import {
  AnthraciteTheme,
  Map,
  Titlebar,
  Toolbar,
  WhiteTheme,
} from 'osm-ui-react';
import * as React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';

import Layers from 'src/apps/bridge/containers/Layers';
import LayersSidebar from 'src/apps/bridge/containers/LayersSidebar';
import SearchSidebar from 'src/apps/bridge/containers/SearchSidebar';
import * as MC from 'src/types/mapcontrib';

const StyledMap = styled(Map)`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 100%;
`;

const GoBackToV1Button = styled(Titlebar)`
  & > div {
    cursor: pointer;
  }
`;

interface IProps {
  currentCenter: MC.LatLng;
  currentZoomLevel: number;
  description: string;
  maxZoomLevel: number;
  minZoomLevel: number;
  name: string;
  setCenter: (center: MC.LatLng) => void;
  setZoom: (zoomLevel: number) => void;
  v1ThemeUrl: string;
  zoomIn: () => void;
  zoomOut: () => void;
}

interface IState {
  isLayersSidebarOpened: boolean;
  isSearchSidebarOpened: boolean;
}

export default class Theme extends React.PureComponent<IProps, IState> {
  public state: IState = {
    isLayersSidebarOpened: false,
    isSearchSidebarOpened: false,
  };

  private mapRef: React.RefObject<any> = React.createRef();

  public render() {
    const {
      currentCenter,
      currentZoomLevel,
      name,
      zoomIn,
      zoomOut,
    } = this.props;
    const { isSearchSidebarOpened, isLayersSidebarOpened } = this.state;

    return (
      <WhiteTheme>
        <StyledMap
          innerRef={this.mapRef}
          center={currentCenter}
          zoom={currentZoomLevel}
          attributionControl={false}
          zoomControl={false}
          onZoomend={this.setZoom}
          onMoveend={this.setCenter}
        >
          <Map.TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="Data &copy; <a href=&quot;http://www.openstreetmap.org/copyright&quot; rel=&quot;noopener noreferrer&quot; target=&quot;_blank&quot;>OpenStreetMap</a>"
            minZoom={0}
            maxZoom={19}
          />
          <Map.AttributionControl position="bottomleft" />
          <Map.ScaleControl position="bottomleft" />
          <Layers />
        </StyledMap>

        {this.renderName(name)}

        <Toolbar opened={true} position="top-left" size="sm" shape="square">
          <Toolbar.Group style={{ marginTop: '70px' }}>
            <Toolbar.Item icon="plus" onClick={zoomIn} />
            <Toolbar.Item icon="minus" onClick={zoomOut} />
            <Toolbar.Item inactive={true}>{currentZoomLevel}</Toolbar.Item>
          </Toolbar.Group>
          <Toolbar.Item icon="layer-group" onClick={this.openLayersSidebar} />
          <Toolbar.Item icon="search" onClick={this.openSearchSidebar} />
        </Toolbar>

        <AnthraciteTheme>
          <a href={this.buildUrlToV1()}>
            <GoBackToV1Button position="bottom-center" size="xs" block={false}>
              Go back to MapContrib v1
            </GoBackToV1Button>
          </a>
        </AnthraciteTheme>

        <LayersSidebar
          opened={isLayersSidebarOpened}
          close={this.closeLayersSidebar}
        />
        <SearchSidebar
          opened={isSearchSidebarOpened}
          close={this.closeSearchSidebar}
          currentCenter={currentCenter}
          currentZoomLevel={currentZoomLevel}
          mapContains={this.mapContains}
        />
      </WhiteTheme>
    );
  }

  private renderName(name: string) {
    if (name) {
      return (
        <>
          <Helmet>
            <title>{name}</title>
          </Helmet>
          <Titlebar position="top-left" size="sm" block={false}>
            {name}
          </Titlebar>
        </>
      );
    }

    return null;
  }

  private setCenter = (event: any) => {
    this.props.setCenter(event.target.getCenter());
  };

  private setZoom = (event: any) => {
    this.props.setZoom(event.target.getZoom());
  };

  private openSearchSidebar = () => {
    this.setState((state: IState) => ({
      ...state,
      isSearchSidebarOpened: true,
    }));
  };

  private closeSearchSidebar = () => {
    this.setState((state: IState) => ({
      ...state,
      isSearchSidebarOpened: false,
    }));
  };

  private openLayersSidebar = () => {
    this.setState((state: IState) => ({
      ...state,
      isLayersSidebarOpened: true,
    }));
  };

  private closeLayersSidebar = () => {
    this.setState((state: IState) => ({
      ...state,
      isLayersSidebarOpened: false,
    }));
  };

  private buildUrlToV1 = () => {
    const v1ThemeUrl = this.props.v1ThemeUrl.replace(/#.*?$/, '');
    const { currentCenter, currentZoomLevel } = this.props;
    return `${v1ThemeUrl}#position/${currentZoomLevel}/${currentCenter.lat}/${
      currentCenter.lng
    }`;
  };

  private mapContains = (latLng: MC.LatLng) => {
    return this.mapRef.current.map.getBounds().contains(latLng);
  };
}
