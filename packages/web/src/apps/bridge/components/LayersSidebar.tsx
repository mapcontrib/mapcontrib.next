import { Form, Sidebar } from 'osm-ui-react';
import * as React from 'react';
import styled from 'styled-components';

import { buildOffMapMarker } from 'src/helpers/ui';
import * as MC from 'src/types/mapcontrib';

const StyledForm = styled(Form)`
  margin-bottom: 2.5rem;
`;

const Card = styled.div`
  display: grid;
  grid-template-columns: 4rem 1fr;
  margin-top: 0.8rem;
  padding: 0 0.4rem;
  border-radius: 0.4rem;
`;

const Marker = styled.div`
  padding-top: 0.6rem;
  padding-right: 0.4rem;
`;

const Layer = styled.section`
  margin: 1rem 0 1rem 0.5rem;
`;

const Title = styled.h2`
  margin-top: 0.1rem;
  margin-bottom: 0.3rem;
  font-size: 1.2rem;
  font-weight: 400;
`;

const FeatureCount = styled.div`
  font-size: 1rem;
  font-style: italic;
  color: #888;
`;

interface IFeatureCount {
  [layerId: string]: number;
}

interface IProps {
  close: () => void;
  featureCount: IFeatureCount;
  mergeMarkerClusters: boolean;
  opened: boolean;
  setMarkerClustersMerge: (mergeMarkerClusters: boolean) => void;
  themeLayers: MC.ThemeLayer[];
}

export default class Theme extends React.PureComponent<IProps> {
  public render() {
    const {
      close,
      featureCount,
      mergeMarkerClusters,
      opened,
      themeLayers,
    } = this.props;

    return (
      <Sidebar opened={opened} onClose={close}>
        <Sidebar.Title>Layers</Sidebar.Title>
        <StyledForm>
          <Form.Group>
            <Form.Checkbox
              id="only-visible"
              value="1"
              checked={mergeMarkerClusters}
              label="Feature clusters contains all the layers"
              onChange={this.onMergeClustersChange}
            />
          </Form.Group>
        </StyledForm>
        <section>{this.renderLayerList(themeLayers, featureCount)}</section>
      </Sidebar>
    );
  }

  private renderLayerList(
    themeLayers: MC.ThemeLayer[],
    featureCount: IFeatureCount,
  ) {
    return themeLayers.map((layer, index) => {
      const count = featureCount[layer.uuid];

      return (
        <Card key={index}>
          <Marker>{buildOffMapMarker(layer)}</Marker>
          <Layer>
            <Title>{layer.name}</Title>
            <FeatureCount>
              {count} {count > 1 ? 'elements' : 'element'} on the map
            </FeatureCount>
          </Layer>
        </Card>
      );
    });
  }

  private onMergeClustersChange = (event: any) => {
    this.props.setMarkerClustersMerge(event.target.checked);
  };
}
