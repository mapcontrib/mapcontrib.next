import { flatten as _flatten, values as _values } from 'lodash';
import * as React from 'react';
import styled from 'styled-components';

import * as MC from 'src/types/mapcontrib';

interface IProps {
  augmentedFeature: MC.AugmentedFeature;
  onClick: (feature: MC.AugmentedFeature) => void;
}

const Card = styled.div`
  display: grid;
  grid-template-columns: 4rem 1fr;
  margin-top: 0.8rem;
  padding: 0 0.4rem;
  border-radius: 0.4rem;
  cursor: pointer;

  &:hover,
  &:focus {
    background: #eee;
  }
`;

const Marker = styled.div`
  padding-top: 0.6rem;
  padding-right: 0.4rem;
`;

const Feature = styled.section`
  margin: 1rem 0 1rem 0.5rem;
`;

const Title = styled.h2`
  margin-top: 0.1rem;
  margin-bottom: 0.3rem;
  font-size: 1.2rem;
  font-weight: 400;
`;

const LayerName = styled.div`
  font-size: 1rem;
  font-style: italic;
  color: #888;
`;

export default class SearchItem extends React.PureComponent<IProps> {
  public render() {
    const { augmentedFeature, onClick, ...props } = this.props;

    return (
      <Card onClick={this.onClick} {...props}>
        <Marker>{augmentedFeature.offMapMarker}</Marker>
        <Feature>
          <Title>{augmentedFeature.feature.tags.name}</Title>
          <LayerName>{augmentedFeature.layer.name}</LayerName>
        </Feature>
      </Card>
    );
  }

  private onClick = () => this.props.onClick(this.props.augmentedFeature);
}
