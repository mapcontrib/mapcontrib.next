import * as Loadable from 'react-loadable';

import LoadingScreen from 'src/components/LoadingScreen';

const Bridge = Loadable({
  loader: () => import('src/apps/bridge/App'),
  loading: LoadingScreen,
});

export default Bridge;
