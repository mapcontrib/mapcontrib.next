import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import Theme from 'src/apps/bridge/components/Theme';
import { setCenter, setZoom, zoomIn, zoomOut } from 'src/stores/theme/actions';
import { IThemeState } from 'src/stores/theme/reducer';
import * as MC from 'src/types/mapcontrib';

interface IMapStateToProps {
  theme: IThemeState;
}

const mapStateToProps = ({ theme }: IMapStateToProps, ownProps = {}) => ({
  currentCenter: theme.currentCenter,
  currentZoomLevel: theme.currentZoomLevel,
  description: theme.theme.description,
  maxZoomLevel: theme.theme.maxZoomLevel,
  minZoomLevel: theme.theme.minZoomLevel,
  name: theme.theme.name,
  v1ThemeUrl: theme.v1ThemeUrl,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setCenter: (center: MC.LatLng) => dispatch(setCenter(center)),
  setZoom: (zoomLevel: number) => dispatch(setZoom(zoomLevel)),
  zoomIn: () => dispatch(zoomIn()),
  zoomOut: () => dispatch(zoomOut()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Theme);
