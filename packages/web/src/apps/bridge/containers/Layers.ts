import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import Layers from 'src/apps/bridge/components/Layers';
import { mergeClustersIfNeededSelector } from 'src/selectors/feature';
import { IBridgeState } from 'src/stores/bridge/reducer';
import { addFeatures } from 'src/stores/geoFeature/actions';
import { IGeoFeatureState } from 'src/stores/geoFeature/reducer';
import { IThemeState } from 'src/stores/theme/reducer';
import * as MC from 'src/types/mapcontrib';

interface IMapStateToProps {
  bridge: IBridgeState;
  geoFeature: IGeoFeatureState;
  theme: IThemeState;
}

const mapStateToProps = (state: IMapStateToProps, ownProps = {}) => {
  const {
    themeLayers,
    themeVisualizationLayers,
  } = mergeClustersIfNeededSelector(state);

  return {
    dataSources: state.theme.dataSources,
    features: state.geoFeature.features,
    themeLayers,
    themeVisualizationLayers,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => ({
  addFeatures: (dataSourceId: MC.Uuid, features: MC.OverpassFeature[]) =>
    dispatch(addFeatures(dataSourceId, features)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Layers);
