import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import SearchSidebar from 'src/apps/bridge/components/SearchSidebar';
import { augmentedFeatureListSelector } from 'src/selectors/feature';
import { IGeoFeatureState } from 'src/stores/geoFeature/reducer';
import { setCenterAndZoom } from 'src/stores/theme/actions';
import { IThemeState } from 'src/stores/theme/reducer';
import * as MC from 'src/types/mapcontrib';

interface IMapStateToProps {
  geoFeature: IGeoFeatureState;
  theme: IThemeState;
}

interface IOwnProps {
  close: () => void;
  currentCenter: MC.LatLng;
  currentZoomLevel: number;
  mapContains: (latLng: MC.LatLng) => boolean;
  opened: boolean;
}

const mapStateToProps = (
  state: IMapStateToProps,
  { close, mapContains, opened }: IOwnProps,
) => ({
  augmentedFeatures: augmentedFeatureListSelector(state),
  close,
  mapContains,
  opened,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setCenterAndZoom: (center: MC.LatLng, zoomLevel: number) =>
    dispatch(setCenterAndZoom(center, zoomLevel)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchSidebar);
