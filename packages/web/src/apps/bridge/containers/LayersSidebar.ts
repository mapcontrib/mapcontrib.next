import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import LayersSidebar from 'src/apps/bridge/components/LayersSidebar';
import { featureCountPerLayerSelector } from 'src/selectors/feature';
import { setMarkerClustersMerge } from 'src/stores/bridge/actions';
import { IBridgeState } from 'src/stores/bridge/reducer';
import { IGeoFeatureState } from 'src/stores/geoFeature/reducer';
import { IThemeState } from 'src/stores/theme/reducer';

interface IMapStateToProps {
  bridge: IBridgeState;
  geoFeature: IGeoFeatureState;
  theme: IThemeState;
}

interface IOwnProps {
  close: () => void;
  opened: boolean;
}

const mapStateToProps = (
  state: IMapStateToProps,
  { close, opened }: IOwnProps,
) => ({
  close,
  featureCount: featureCountPerLayerSelector(state),
  mergeMarkerClusters: state.bridge.mergeMarkerClusters,
  opened,
  themeLayers: state.theme.themeLayers,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setMarkerClustersMerge: (mergeMarkerClusters: boolean) =>
    dispatch(setMarkerClustersMerge(mergeMarkerClusters)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LayersSidebar);
