import rootSaga from 'src/sagas/root';
import bridgeReducer from 'src/stores/bridge/reducer';
import configureStore from 'src/stores/configure';
import geoFeatureReducer from 'src/stores/geoFeature/reducer';
import themeReducer from 'src/stores/theme/reducer';

const initialState = {};
const reducers = {
  bridge: bridgeReducer,
  geoFeature: geoFeatureReducer,
  theme: themeReducer,
};

export default configureStore(reducers, initialState, rootSaga);
