[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![pipeline status](https://gitlab.com/mapcontrib/mapcontrib.next/badges/develop/pipeline.svg)](https://gitlab.com/mapcontrib/mapcontrib.next/commits/develop)
[![coverage report](https://gitlab.com/mapcontrib/mapcontrib.next/badges/develop/coverage.svg)](https://gitlab.com/mapcontrib/mapcontrib.next/commits/develop)
[![config: knuckle](https://img.shields.io/badge/config-knuckle-ff5c00.svg?style=flat-square)](https://github.com/GuillaumeAmat/knuckle)

# MapContrib Next

It is a complete rewrite of the MapContrib project using [React](https://reactjs.org), [Redux](http://redux.js.org) and the [OSM UI project](https://github.com/osm-ui).

The main reasons for that are to improve the development experience, to make the interfaces available to the community (via OSM UI) and to make a better MapContrib for all of us.

MapContrib Next will be the 2.0.0 version of the MapContrib project so it will be merged into the main repository when the features will be completed.


## Contribute

See [CONTRIBUTING.md](CONTRIBUTING.md)
